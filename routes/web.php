<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\HomePageController@index')->name('index');
Route::get('/category/{id}', 'Frontend\HomePageController@showByCategory')->name('by.category');
Route::get('/Not-Found', 'Frontend\HomePageController@pageNotFound')->name('pagenotfound');


Route::group(['prefix' => '/ControlPanel'],function () {
    Route::group(['prefix' => 'admins'], function (){
        Route::get('/list', 'Backend\AdminController@index')->name('admin.index');
        Route::get('/create/form', 'Backend\AdminController@addAdminForm')->name('admin.create.form');
        Route::post('/create', 'Auth\RegisterController@register')->name('admin.create');
        //    admin email verify  route
        Route::get('/token/{token}','Backend\AdminController@verify')->name('admin.verify');

        Route::get('/edit/{id}','Backend\AdminController@edit')->name('admin.edit');
        Route::post('/update/{id}','Backend\AdminController@update')->name('admin.update');
        Route::get('/delete/{id}','Backend\AdminController@delete')->name('admin.delete');
        Route::get('/block/{id}','Backend\AdminController@block')->name('admin.block');
        Route::get('/unblock/{id}','Backend\AdminController@unblock')->name('admin.unblock');
    });
    Route::group(['prefix' => 'category'],function () {
        Route::get('/', 'Backend\CategoryController@index')->name('admin.category.index');
        Route::get('/create', 'Backend\CategoryController@create')->name('admin.category.create');
        Route::post('/store', 'Backend\CategoryController@store')->name('admin.category.store');
        Route::get('/edit/{id}', 'Backend\CategoryController@edit')->name('admin.category.edit');
        Route::post('/update/{id}', 'Backend\CategoryController@update')->name('admin.category.update');
        Route::get('/active/{id}', 'Backend\CategoryController@active')->name('admin.category.active');
        Route::get('/inactive/{id}', 'Backend\CategoryController@inactive')->name('admin.category.inactive');
        Route::get('/delete/{id}', 'Backend\CategoryController@delete')->name('admin.category.delete');
    });
    Route::group(['prefix' => 'portfolio'],function () {
        Route::get('/', 'Backend\PortfolioController@index')->name('admin.portfolio.index');
        Route::get('/create', 'Backend\PortfolioController@create')->name('admin.portfolio.create');
        Route::post('/store', 'Backend\PortfolioController@store')->name('admin.portfolio.store');
        Route::get('/edit/{id}', 'Backend\PortfolioController@edit')->name('admin.portfolio.edit');
        Route::post('/update/{id}', 'Backend\PortfolioController@update')->name('admin.portfolio.update');
        Route::get('/add/{id}', 'Backend\PortfolioController@add')->name('admin.portfolio.add');
        Route::get('/remove/{id}', 'Backend\PortfolioController@remove')->name('admin.portfolio.remove');
        Route::get('/delete/{id}', 'Backend\PortfolioController@delete')->name('admin.portfolio.delete');
    });
    Route::group(['prefix' => 'logo'],function () {
        Route::get('/edit/{id}', 'backend\AdminHomePageController@logoEdit')->name('admin.logo.edit');
        Route::post('/update/{id}', 'backend\AdminHomePageController@logoUpdate')->name('admin.logo.update');
    });

    Auth::routes();
    Route::get('/home', 'backend\AdminHomePageController@index')->name('home');

});

