<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\User;

class VerifyRegistration extends Notification
{
    use Queueable;
    public $admin;
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $admin,$token){
        $this->admin = $admin;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('The Imran Admin Confirmation Email')
            ->line('Making you The Imran admin.')
            ->line('If you agree Please confirm your email by clicking the link below.')
            ->action('Confirm Registration', route('admin.verify',$this->token))
            ->line('Thank you for Your Request to be an admin!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
