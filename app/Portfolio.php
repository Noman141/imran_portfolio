<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model{
    protected $fillable = [
        'admin_id','category_id','image','status'
    ];

    public function admin(){
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }

}
