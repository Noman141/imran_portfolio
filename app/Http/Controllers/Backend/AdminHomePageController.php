<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Logo;
use Illuminate\Http\Request;use Intervention\Image\Facades\Image as Image;
use File;

class AdminHomePageController extends Controller{
    public function __construct(){
        $this->middleware('auth:web');
    }
    public function index(){
        return view('backend.pages.index');
    }

    public function logoEdit($id){
        $logo = Logo::find($id);
        return view('backend.pages.logo.edit',compact('logo'));
    }

    public function logoUpdate(Request $request, $id){
        $logo = Logo::find($id);
        //        delete old image
        if ($request->image > 0) {
            if (File::exists('images/logo/' . $logo->image)) {
                File::delete('images/logo/' . $logo->image);
            }
        }
        //insert image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'AlImran-Logo'.'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/logo/'.$image_name);
            Image::make($image)->save($location);
            $logo->image = $image_name;
        }
        $logo->save();
        session()->flash('success','Logo Has Been Updated');
        return redirect()->route('home');
    }
}
