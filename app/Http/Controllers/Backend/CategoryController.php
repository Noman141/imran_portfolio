<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller{
    public function __construct(){
        $this->middleware('auth:web');
    }

    public function index(){
        $categories = Category::orderBy('id','desc')->get();
        return view('backend.pages.category.index',compact('categories'));
    }

    public function create(){
        return view('backend.pages.category.create');
    }

    public function store(Request $request){
        $request->validate([
           'name'   => 'required|max:255|string|unique:categories'
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->save();

        session()->flash('success','Category Created Successfully');
        return redirect()->route('admin.category.index');
    }

    public function edit($id){
        $category = Category::find($id);
        return view('backend.pages.category.edit',compact('category'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'name'   => 'required|max:255|string|unique:categories'
        ]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->save();

        session()->flash('success','Category Has Updated Successfully');
        return redirect()->route('admin.category.index');
    }

    public function active($id){
        $category = Category::find($id);
        $category->status = 0;
        $category->save();

        session()->flash('success','The Category Has Been Successfully Activated');
        return redirect()->route('admin.category.index');
    }

    public function inactive($id){
        $category = Category::find($id);
        $category->status = 1;
        $category->save();

        session()->flash('success','The Category Has Been Successfully deactivated');
        return redirect()->route('admin.category.index');
    }

    public function delete($id){
        $category = Category::find($id);
        $category->delete();
        session()->flash('success','The Category Has Been Deleted Successfully');
        return redirect()->route('admin.category.index');
    }
}
