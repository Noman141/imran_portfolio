<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Portfolio;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use File;

class PortfolioController extends Controller{
    public function __construct(){
        $this->middleware('auth:web');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $portfolios = Portfolio::orderBy('id','desc')->get();
        return view('backend.pages.portfolio.index',compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $categories = Category::where('status',0)->get();
        return view('backend.pages.portfolio.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
           'admin_id'   =>'numeric|required',
           'category_id'   =>'numeric|required',
           'status'   =>'numeric',
           'image'   =>'image|required|mimes:jpeg,bmp,png|max:1024',
        ]);

        $portfolio = new Portfolio();

        $portfolio->admin_id = $request->admin_id;
        $portfolio->category_id = $request->category_id;
        $portfolio->status = $request->status;

        //insert image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'TheAlImran-portfolio-Image'.'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/portfolios/'.$image_name);
            Image::make($image)->save($location);
            $portfolio->image = $image_name;
        }
        $portfolio->save();
        session()->flash('success','Portfolio Has Been Created Successfully');
        return redirect()->route('admin.portfolio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $portfolio = Portfolio::find($id);
        $categories = Category::where('status',0)->get();
        return view('backend.pages.portfolio.edit',compact('portfolio','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'admin_id'   =>'numeric|required',
            'category_id'   =>'numeric|required',
            'status'   =>'numeric',
            'image'   =>'image|mimes:jpeg,bmp,png|max:1024',
        ]);

        $portfolio = Portfolio::find($id);

        $portfolio->admin_id = $request->admin_id;
        $portfolio->category_id = $request->category_id;
        $portfolio->status = $request->status;

        //        delete old image
        if ($request->image > 0) {
            if (File::exists('images/portfolios/' . $portfolio->image)) {
                File::delete('images/portfolios/' . $portfolio->image);
            }
        }
        //insert image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'AlImran'.'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/portfolios/'.$image_name);
            Image::make($image)->save($location);
            $portfolio->image = $image_name;
        }
        $portfolio->save();
        session()->flash('success','Portfolio Has Been Updated Successfully');
        return redirect()->route('admin.portfolio.index');
    }

    public function add($id){
        $portfolio = Portfolio::find($id);
        $portfolio->status =1;
        $portfolio->save();
        session()->flash('success','The Portfolio Has Been Added Successfully');
        return redirect()->route('admin.portfolio.index');
    }

    public function remove($id){
        $portfolio = Portfolio::find($id);
        $portfolio->status =0;
        $portfolio->save();
        session()->flash('success','The Portfolio Has Been Removed Successfully');
        return redirect()->route('admin.portfolio.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $portfolio = Portfolio::find($id);

        if (File::exists('images/portfolios/' . $portfolio->image)) {
            File::delete('images/portfolios/' . $portfolio->image);
        }


        $portfolio->delete();
        session()->flash('success','The Portfolio Has Been Deleted Successfully');
        return redirect()->route('admin.portfolio.index');
    }
}
