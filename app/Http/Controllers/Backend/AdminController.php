<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Intervention\Image\Facades\Image as Image;
use File;
use Illuminate\Support\Str;

class AdminController extends Controller{
    public function __construct(){
        $this->middleware('auth:web');
    }

    public function index(){
        $admins = User::orderBy('id','asc')->get();
        return view('backend.pages.admin.index',compact('admins'));
    }

    public function addAdminForm(){
        return view('backend.pages.admin.create');
    }

    public function edit($id){
        $admin = User::find($id);
        return view('backend.pages.admin.edit',compact('admin'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'role' => 'required|max:255|string',
            'username' => 'required|max:255|string',
            'phone' => 'nullable|numeric',
            'image' => 'nullable|image|mimes:jpeg,jpg,png',
        ]);

        $admin = User::find($id);
        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->phone = $request->phone;
        $admin->role = $request->role;
        $admin->slug = Str::slug($request->username);

        //        delete old image
        if ($request->image > 0) {
            if (File::exists('images/admin/' . $admin->image)) {
                File::delete('images/admin/' . $admin->image);
            }
        }
        //insert image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'AlImran'.'-'.$admin->username.'.'.$image->getClientOriginalExtension();
            $location = public_path('images/admins/'.$image_name);
            Image::make($image)->save($location);
            $admin->image = $image_name;
        }
        $admin->save();
        session()->flash('success','Profile Has Updated');
        return redirect()->route('admin.index',$admin->id);
    }


    public function verify($token){
        $admin = User::where('remember_token',$token)->first();

        if (!is_null($admin)){
            $admin->status =1;
            $admin->remember_token = NULL;
            $admin->email_verified_at = time();

            $admin->save();
            session()->flash('success','Your Verification Successful');
            return redirect()->route('login');
        }else{
            session()->flash('errormsg','Sorry! Your token is not matched!!! ');
            return redirect()->route('login');
        }
    }

    public function block($id){
        $admin = User::find($id);
        $admin->status =2;
        $admin->save();

        session()->flash('success','Blocked Done');
        return redirect()->route('admin.index');
    }

    public function unblock($id){
        $admin = User::find($id);
        $admin->status =1;
        $admin->save();

        session()->flash('success','Unblocked Done');
        return redirect()->route('admin.index');
    }


    public function delete($id){
        $admin = User::find($id);
        if (!is_null($admin)){
//          Delete Category Image
            if (File::exists('images/admins/'.$admin->image)){
                File::delete('images/admins/'.$admin->image);
            }
            $admin->delete();
        }

        session()->flash('success','Admin Has Deleted Successfully');

        return redirect()->route('admin.index');
    }

}
