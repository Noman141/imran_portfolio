<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\VerifyRegistration;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use File;
use Auth;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    public $image;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '/ControlPanel/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth:web');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request){

        $request->validate([

            'username' => 'required|max:255|string|unique:users',
            'email' => 'required|string|email|string|unique:users',
            'password' => 'required|min:8|string|confirmed',
            'role' => 'required|string',
            'phone' => 'nullable|numeric|unique:users',
            'image' => 'nullable|image',
        ]);
        if ($request->image >0){
            $image = $request->file('image');
            $image_name = 'The_imran'.'-'.'admin-'.$request->username.'.'.$image->getClientOriginalExtension();
            $location = public_path('images/admins/'.$image_name);
            Image::make($image)->save($location);
            $this->image = $image_name;
        }
        $admin = new User();
        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->phone = $request->phone;
        $admin->role = $request->role;
        $admin->image = $this->image;
        $admin->password = Hash::make($request['password']);
        $admin->remember_token = Str::random(50);
        $admin->status = 0;
        $admin->slug = Str::slug($request->username);

        $admin->save();
        $admin->notify(new VerifyRegistration($admin,$admin->remember_token));

        session()->flash('success','A confirmation message has sent to the new admins Email');
        return redirect()->route('admin.index');
    }
}
