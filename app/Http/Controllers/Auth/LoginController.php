<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/ControlPanel/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function login(Request $request){

        $request->validate([
            'email'    => 'required|email',
            'password'    => 'required',
        ]);

//        Find user By this email
        $admin = User::where('email',$request->email)->first();

        if (!isset($admin)) {
            session()->flash('errormsg', 'You are not registered. Please Register First');
            return redirect()->route('login');
        }elseif ($admin->status == 1) {
//            log him now
            if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
                return redirect()->intended(route('home'));
            }else {
                session()->flash('errormsg', 'Something wents wrong!!!Please try again');
                return redirect()->route('login');

            }
        }elseif ($admin->status == 0){
            session()->flash('errormsg', 'Your Email is not verifyed. Please check your email and verify your email');
            return redirect()->route('login');
        }elseif ($admin->status == 2){
            session()->flash('errormsg', 'Your Blocked. Please Contact With Super Admin');
            return redirect()->route('login');
        }else {
            session()->flash('errormsg', 'Something wents wrong!!!Please try again');
            return redirect()->route('admin.login');

        }
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

}
