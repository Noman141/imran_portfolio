<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Portfolio;
use Illuminate\Http\Request;

class HomePageController extends Controller{
    public function index(){
        $categories = Category::where('status',0)->get();
        $portfolios = Portfolio::orderBy('id','desc')->where('status',1)->limit(30)->get();
        return view('frontend.pages.index',compact('categories','portfolios'));
    }

    public function showByCategory($slug){
        $category = Category::where('slug',$slug)->first();
        $portfolios = Portfolio::orderBy('id','desc')->where('category_id',$category->id)->get();
        return view('frontend.pages.showbycategory',compact('category','portfolios'));
    }
    public function pageNotFound(){
        return view('frontend.pages.404');
    }
}
