<!DOCTYPE html>
<html lang="en">
<head>
    <title>The Imran - @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('frontend.partials.styles')
</head>
<body>
@yield('content')

@include('frontend.partials.js')
</body>
</html>