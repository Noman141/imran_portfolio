<script src="{{ asset('js/frontend/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery-ui.js') }}"></script>
<script src="{{ asset('js/frontend/popper.min.js') }}"></script>
<script src="{{ asset('js/frontend/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/frontend/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/frontend/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/frontend/aos.js') }}"></script>
<script src="{{ asset('js/backend/all.min.js') }}"></script>

<script src="{{ asset('js/frontend/jquery.fancybox.min.js') }}"></script>

<script src="{{ asset('js/frontend/main.js') }}"></script>