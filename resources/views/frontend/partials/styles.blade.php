<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('fonts/frontend/icomoon/style.css') }}">

<link rel="stylesheet" href="{{ asset('css/frontend/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/owl.theme.default.min.css') }}">

<link rel="stylesheet" href="{{ asset('css/frontend/bootstrap-datepicker.css') }}">

<link rel="stylesheet" href="{{ asset('fonts/frontend/flaticon/font/flaticon.css') }}">

<link rel="stylesheet" href="{{ asset('css/frontend/aos.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/fancybox.min.css') }}">
<link href="{{ asset('css/backend/all.min.css') }}" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">