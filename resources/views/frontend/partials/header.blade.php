<header>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark main-nav py-4">
        <div class="container">
            <!-- Navbar brand -->
            <a class="navbar-brand mt-1" href="{{ route('index') }}"><img src="{{ asset('images/logo/'.\App\Logo::first()->image) }}"></a>
            <!-- Collapse button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>Menu</button>
            <!-- Collapsible content -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Links -->
                <ul class="navbar-nav m-auto">
                    <li class="nav-item ">
                        <a class="nav-link px-4" data-scroll href="{{ route('index') }}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    @foreach(\App\Category::where('status',0)->get() as $category)
                    <li class="nav-item">
                        <a class="nav-link px-4" data-scroll href="{{ route('by.category',$category->id) }}">{{ $category->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <!-- Collapsible content -->
        </div>
    </nav>
    <!--/.Navbar-->
</header>