@extends('frontend.layouts.master')

@section('title')
    Home
@endsection

@section('content')
@include('frontend.partials.header')
<main class="main-content mr-auto">
    <div class="container-fluid photos">
        <div class="row align-items-stretch">
            @foreach($portfolios as $portfolio)
            <div class="col-md-6" data-aos="fade-up" data-aos-delay="100">
                <a href="{{ route('by.category',$portfolio->category->slug) }}" class="d-block photo-item">
                    <img src="{{ asset('images/portfolios/'.$portfolio->image) }}" alt="Image">
                    <div class="photo-text-more">
                        <div class="photo-text-more">
                            <h3 class="heading">{{ $portfolio->category->name }}</h3>
                            <span class="meta">{{ \App\Portfolio::where('category_id',$portfolio->category->id)->count() }} {{ \App\Portfolio::where('category_id',$portfolio->category->id)->count()==1?'Photo':'Photos' }}</span>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        @include('frontend.partials.footer')
    </div>
</main>

@endsection