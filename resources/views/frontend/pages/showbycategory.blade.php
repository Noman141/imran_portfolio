@extends('frontend.layouts.master')

@section('title')
    Home
@endsection

@section('content')
    @include('frontend.partials.header')
    <main class="main-content">
        <div class="container-fluid photos">

            <div class="row pt-4 mb-5 text-center">
                <div class="col-12">
                    <h2 class="text-white mb-4">'{{ $category->name }}' &mdash; {{ $portfolios->count() }} {{ $portfolios->count()==1?'Photo':'Photos' }}</h2>
                </div>
            </div>

            <div class="row align-items-stretch">
                @foreach($portfolios as $portfolio)
                <div class="col-6 col-md-6" data-aos="fade-up" data-aos-delay="100">
                    <a href="{{ asset('images/portfolios/'.$portfolio->image) }}" class="d-block photo-item" data-fancybox="gallery">
                        <img src="{{ asset('images/portfolios/'.$portfolio->image) }}" alt="Image" class="img-fluid">
                        <div class="photo-text-more">
                            <i class="fas fa-search-plus"></i>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>


            @include('frontend.partials.footer')
        </div>
    </main>

@endsection