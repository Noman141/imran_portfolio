<!-- Bootstrap core JavaScript-->
<script src="{{ asset('js/backend/jquery.min.js') }}"></script>
<script src="{{ asset('js/backend/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('js/backend/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/backend/all.min.js') }}"></script>

@yield('scripts')
<!-- Custom scripts for all pages-->
<script src="{{ asset('js/backend/sb-admin.min.js') }}"></script>