<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#manageAdmin" aria-expanded="false" aria-controls="manageAdmin">  <i class="fas fa-fw fa-user"></i> <span class="menu-title">Manage Admins</span><i class="fas fa-angle-double-down ml-3"></i></a>
        <div class="collapse" id="manageAdmin">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="{{ route('admin.index') }}"><i class="fas fa-list mr-3"></i>Admin List</a></li>

                {{--@if(Auth::user()->role == 'Super-Admin' || Auth::user()->role == 'Admin')--}}
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.create.form') }}"><i class="fas fa-user-plus mr-3"></i>Admin Create</a></li>
                {{--@endif--}}
            </ul>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#manageCategory" aria-expanded="false" aria-controls="manageCategory">  <i class="fas fa-compact-disc"></i> <span class="menu-title">Manage Category</span><i class="fas fa-angle-double-down ml-3"></i></a>
        <div class="collapse" id="manageCategory">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="{{ route('admin.category.index') }}"><i class="fas fa-list mr-3"></i>Category List</a></li>

                {{--@if(Auth::user()->role == 'Super-Admin' || Auth::user()->role == 'Admin')--}}
                <li class="nav-item"> <a class="nav-link" href="{{ route('admin.category.create') }}"><i class="fas fa-plus-circle mr-3"></i>Create Category</a></li>
                {{--@endif--}}
            </ul>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#managePortfolio" aria-expanded="false" aria-controls="managePortfolio">  <i class="fas fa-images"></i> <span class="menu-title">Manage Portfolio</span><i class="fas fa-angle-double-down ml-3"></i></a>
        <div class="collapse" id="managePortfolio">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="{{ route('admin.portfolio.index') }}"><i class="fas fa-sliders-h mr-3"></i>Portfolio List</a></li>

                {{--@if(Auth::user()->role == 'Super-Admin' || Auth::user()->role == 'Admin')--}}
                <li class="nav-item"> <a class="nav-link" href="{{ route('admin.portfolio.create') }}"><i class="fas fa-cloud-upload-alt mr-3"></i>Portfolio Create</a></li>
                {{--@endif--}}
            </ul>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.logo.edit',\App\Logo::first()->id) }}"><i class="fas fa-cloud-upload-alt "></i>Update Logo</a>
    </li>

    <li class="nav-item">
        <form action="{{ asset(route('logout')) }}" class="form-inline" method="post">
            @csrf
            <input type="submit" value="Log Out" class="btn btn-danger">
        </form>
    </li>
</ul>