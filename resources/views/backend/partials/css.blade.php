<!-- Custom fonts for this template-->
<link href="{{ asset('css/backend/all.min.css') }}" rel="stylesheet" type="text/css">

@yield('style-sheet')
<!-- Custom styles for this template-->
<link href="{{ asset('css/backend/sb-admin.css') }}" rel="stylesheet">
