<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>The Imran - @yield('title')</title>
  @include('backend.partials.css')
</head>
   @include('backend.partials.header')
<body id="page-top">


  <div id="wrapper">

      @include('backend.partials.footer')
      @include('backend.partials.sidebar')
      @yield('content')

  </div>



@include('backend.partials.js')
</body>

</html>
