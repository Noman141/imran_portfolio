@extends('backend.layouts.master')
@section('title')
    | Create
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style-sheet')

@endsection

@section('content')
    <div class="container mb-5">
        <!-- Breadcrumbs-->
        <div class="row breadcrumb">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Create Admin</li>
                </ol>
            </div>
            <div class="col-md-2">
                <a href="{{ route('admin.index') }}" class="btn btn-info" style="color: #fff">Admin List</a>
            </div>
        </div>
        <div class="card card-register mx-auto m-5">
            <div class="card-header">Create A New Admin</div>
            <div class="card-body">
                @include('global.msg')
                <form action="{{ route('admin.create') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="text" id="username" name="username" class="form-control" placeholder="User Name" required="required">
                            <label for="username">User Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required="required">
                            <label for="inputEmail">Email address</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-label-group">
                                    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="required">
                                    <label for="inputPassword">Password</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-label-group">
                                    <input name="password_confirmation" type="password" id="confirmPassword" class="form-control" placeholder="Confirm password" required="required">
                                    <label for="confirmPassword">Confirm password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-label-group">
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone Number" required="required">
                                    <label for="phone">Phone</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-label-group">
                                    <select class="form-control form-control-lg" name="role" style="line-height: 1.5;color: #495057;">
                                        <option>Admin Role</option>
                                        <option value="Super-Admin" >Admin</option>
                                        <option value="Author" >Author</option>
                                        <option value="Editor" >Editor</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="file" name="image" id="image" class="form-control" placeholder="Upload Profile Image" required="required">
                            <label for="image">Profile Image</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
