@extends('backend.layouts.master')
@section('title')
    | Edit
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style-sheet')

@endsection

@section('content')
    <div class="container mb-5">
        <!-- Breadcrumbs-->
        <div class="row breadcrumb">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Update Admin</li>
                </ol>
            </div>
            <div class="col-md-2">
                <a href="{{ route('admin.index') }}" class="btn btn-info" style="color: #fff">Admin List</a>
            </div>
        </div>
        <div class="card card-register mx-auto m-5">
            <div class="card-header">Update Admin</div>
            <div class="card-body">
                @include('global.msg')
                <form action="{{ route('admin.update',$admin->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="avatar" style="text-align: center; margin-bottom: 15px"><img style="text-align: center" src="{{ asset('images/admins/'.$admin->image) }}" class="rounded-circle" alt="{{ $admin['name'] }}"></div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="text" id="username" name="username" value="{{ $admin->username }}" class="form-control" placeholder="User Name" required="required">
                            <label for="username">User Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" name="email" value="{{ $admin->email }}" class="form-control" placeholder="Email address" required="required">
                            <label for="inputEmail">Email address</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-label-group">
                                    <input type="text" name="phone" id="phone" value="{{ $admin->phone }}" class="form-control" placeholder="Phone Number" required="required">
                                    <label for="phone">Phone</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-label-group">
                                    <select class="form-control form-control-lg" name="role" style="line-height: 1.5;color: #495057;">
                                        <option>Admin Role</option>
                                        <option {{ $admin->role == 'Super-Admin'?'selected':'' }} value="Super-Admin" >Admin</option>
                                        <option {{ $admin->role == 'Author'?'selected':'' }}  value="Author" >Author</option>
                                        <option {{ $admin->role == 'Editor'?'selected':'' }} value="Editor" >Editor</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="file" name="image" id="image" class="form-control" placeholder="Upload Profile Image">
                            <label for="image">Profile Image</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
