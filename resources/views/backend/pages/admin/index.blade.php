@extends('backend.layouts.master')
@section('title')
    | List
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style-sheet')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
@endsection

@section('content')
        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <div class="row breadcrumb">
                <div class="col-md-10">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Admin List</li>
                    </ol>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('admin.create.form') }}" class="btn btn-info" style="color: #fff">Create Admin </a>
                </div>
            </div>
            @include('global.msg')
            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($admins  as $admin)
                                <tr>
                                    <td scope="row">{{ $loop->index+1 }}</td>
                                    <td>{{ $admin->username }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td>{{ $admin->role }}</td>
                                    <td>{{ $admin->phone }}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm" href="{{ route('admin.edit',$admin->id) }}" style="color: #fff">Edit</a>
                                        @if($admin->status == 1 OR 0)
                                        <a class="btn btn-warning btn-sm" href="{{ route('admin.block',$admin->id) }}" style="color: #fff">Block</a>
                                        @elseif($admin->status == 2)
                                        <a class="btn btn-success btn-sm" href="{{ route('admin.unblock',$admin->id) }}" style="color: #fff">Unblock</a>
                                        @endif
                                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{ $admin->id }}">Delete</button>
                                    </td>
                                    <!--Delete Modal -->
                                    <div class="modal fade" id="exampleModal{{ $admin->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h2 class="modal-title" id="exampleModalLabel" style="color: red;margin-left: auto">Are You Sure!</h2>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h4 style="color: red;text-align: center">Want To Delete <strong>{{ $admin->username }} !</strong></h4>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <a href="{{ route('admin.delete',$admin->id) }}" style="color:#fff;" class="btn btn-danger">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->

@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#dataTable').DataTable();
        } );
    </script>
@endsection
