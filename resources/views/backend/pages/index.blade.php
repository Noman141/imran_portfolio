@extends('backend.layouts.master')
@section('title')
    Dashboard
@endsection
@section('content')
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <div class="container" style="text-align: center;margin-top: 100px">
        @include('global.msg')

        <h2>Hello {{ Auth::user()->username }}</h2>
        <h3>Welcome to the Admin panel</h3>
    </div>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <form action="{{ asset(route('logout')) }}" class="form-inline" method="post">
                        @csrf
                        <input type="submit" value="Log Out" class="btn btn-danger">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
