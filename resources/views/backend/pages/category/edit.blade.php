@extends('backend.layouts.master')
@section('title')
    | Category - Edit
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style-sheet')

@endsection

@section('content')
    <div class="container mb-5">
        <!-- Breadcrumbs-->
        <div class="row breadcrumb">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Edit Category</li>
                </ol>
            </div>
            <div class="col-md-2">
                <a href="{{ route('admin.category.index') }}" class="btn btn-info" style="color: #fff">Category List</a>
            </div>
        </div>
        <div class="card card-register mx-auto m-5">
            <div class="card-header">Create A New Category</div>
            <div class="card-body">
                @include('global.msg')
                <form action="{{ route('admin.category.update',$category->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="text" id="name" name="name" class="form-control" value="{{ $category->name }}" required="required">
                            <label for="name">Category Name</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Update</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
