@extends('backend.layouts.master')
@section('title')
    | Portfolio - List
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style-sheet')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
@endsection

@section('content')
    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <div class="row breadcrumb">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Portfolio List</li>
                </ol>
            </div>
            <div class="col-md-2">
                <a href="{{ route('admin.portfolio.create') }}" class="btn btn-info" style="color: #fff">Create Portfolio </a>
            </div>
        </div>
    @include('global.msg')
    <!-- DataTables Example -->
        <div class="card mb-3">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th></th>
                            <th>Category Name</th>
                            <th>Added By</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($portfolios  as $portfolio)
                            <tr>
                                <td scope="row">{{ $loop->index+1 }}</td>
                                <td><img src="{{ asset('images/Portfolios/'.$portfolio->image) }}" style="height: 100px; width: 200px" alt=""></td>
                                <td>{{ $portfolio->category->name }}</td>
                                <td>{{ $portfolio->admin->username }}</td>
                                <td>{{ $portfolio->status==1?'True':'False' }}</td>
                                <td>
                                    <a class="btn btn-info btn-sm" href="{{ route('admin.portfolio.edit',$portfolio->id) }}" style="color: #fff">Edit</a>
                                    @if($portfolio->status == 1)
                                        <a class="btn btn-warning btn-sm" href="{{ route('admin.portfolio.remove',$portfolio->id) }}" style="color: #fff">Remove From Home</a>
                                    @elseif($portfolio->status == 0)
                                        <a class="btn btn-success btn-sm" href="{{ route('admin.portfolio.add',$portfolio->id) }}" style="color: #fff">Add To Home</a>
                                    @endif
                                    <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{ $portfolio->id }}">Delete</button>
                                </td>
                                <!--Delete Modal -->
                                <div class="modal fade" id="exampleModal{{ $portfolio->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h2 class="modal-title" id="exampleModalLabel" style="color: red;margin-left: auto">Are You Sure!</h2>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <h4 style="color: red;text-align: center">Want To Delete <strong>{{ $portfolio->name }} !</strong></h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <a href="{{ route('admin.portfolio.delete',$portfolio->id) }}" style="color:#fff;" class="btn btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
    <!-- /.container-fluid -->

@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#dataTable').DataTable();
        } );
    </script>
@endsection
