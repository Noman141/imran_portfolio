@extends('backend.layouts.master')
@section('title')
    | Category - Create
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style-sheet')

@endsection

@section('content')
    <div class="container mb-5">
        <!-- Breadcrumbs-->
        <div class="row breadcrumb">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Create Portfolio</li>
                </ol>
            </div>
            <div class="col-md-2">
                <a href="{{ route('admin.category.index') }}" class="btn btn-info" style="color: #fff">Portfolio List</a>
            </div>
        </div>
        <div class="card card-register mx-auto m-5">
            <div class="card-header">Create A New Portfolio</div>
            <div class="card-body">
                @include('global.msg')
                <form action="{{ route('admin.portfolio.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" class="form-control" id="admin_id"  name="admin_id" value="{{ Auth::user()->id }}">

                    <div class="form-group">
                        <div class="form-label-group">
                            <select class="form-control form-control-lg" name="category_id" style="line-height: 1.5;color: #495057;">
                                <option>Select A Category---</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}" >{{ $category->name }}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <div class="form-label-group">
                                <input type="file" name="image" id="image" class="form-control" placeholder="Upload Profile Image" required="required">
                                <label for="image">Profile Image</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control form-control-lg" name="status" style="line-height: 1.5;color: #495057;">
                            <option>Want To Add This Homepage---</option>
                                <option value="1" >Yes</option>
                                <option value="0" >No</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
