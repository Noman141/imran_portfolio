@extends('backend.layouts.master')
@section('title')
    | Logo - Update
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style-sheet')

@endsection

@section('content')
    <div class="container mb-5">
        <!-- Breadcrumbs-->
        <div class="row breadcrumb">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Update Logo</li>
                </ol>
            </div>
        </div>
        <div class="card card-register mx-auto m-5">
            <div class="card-header">Update Your Logo</div>
            <div class="card-body">
                @include('global.msg')
                <form action="{{ route('admin.logo.update',$logo->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <div class="form-label-group">
                                <h5>Current Logo</h5>
                                <img src="{{ asset('images/logo/'.$logo->image) }}" alt="" height="100px" width="100px" style="margin-left: 35%">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <div class="form-label-group">
                                <input type="file" name="image" id="image" class="form-control" placeholder="Upload Profile Image">
                                <label for="image">Logo Image</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Update</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
