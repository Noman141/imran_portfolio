-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2019 at 10:46 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thealimran`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Logo design', 'logo-design', 0, NULL, '2019-11-27 11:01:20'),
(3, 'T-Shirt', 't-shirt', 0, '2019-11-27 11:17:37', '2019-11-27 11:19:53'),
(4, 'Tech', 'tech', 1, '2019-11-27 11:19:46', '2019-11-28 11:37:53'),
(5, 'Banner Design', 'banner-design', 0, '2019-11-28 13:08:29', '2019-11-28 13:08:29'),
(6, 'Packaging Design', 'packaging-design', 1, '2019-11-28 13:08:42', '2019-11-28 13:11:51'),
(7, 'Ui/Ux', 'uiux', 0, '2019-11-28 13:08:53', '2019-11-28 13:08:53'),
(8, 'Education', 'education', 1, '2019-11-28 13:09:19', '2019-11-28 13:11:54'),
(9, 'Business Card', 'business-card', 0, '2019-11-28 13:09:30', '2019-11-28 13:09:30'),
(10, 'Background Removal', 'background-removal', 1, '2019-11-28 13:10:08', '2019-11-28 13:12:05'),
(11, 'Web Banner', 'web-banner', 0, '2019-11-28 13:10:34', '2019-11-28 13:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, 'AlImran-Logo-1574939274.png', NULL, '2019-11-28 05:07:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2019_08_19_000000_create_failed_jobs_table', 1),
(10, '2019_11_26_131315_create_categories_table', 1),
(12, '2019_11_27_172203_create_portfolios_table', 2),
(13, '2019_11_28_104307_create_logos_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `admin_id`, `category_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'AlImran-1574888041.png', 1, NULL, '2019-11-28 11:44:14'),
(2, 1, 3, 'AlImran-1574888063.jpg', 1, '2019-11-27 14:18:50', '2019-11-27 15:03:21'),
(3, 1, 4, 'AlImran-1574887982.jpg', 1, '2019-11-27 14:26:18', '2019-11-27 14:53:02'),
(5, 1, 5, 'TheAlImran-portfolio-Image-1574968366.jpg', 1, '2019-11-28 13:12:46', '2019-11-28 13:15:45'),
(6, 1, 7, 'TheAlImran-portfolio-Image-1574968523.jpg', 1, '2019-11-28 13:15:23', '2019-11-28 13:15:23'),
(8, 1, 11, 'TheAlImran-portfolio-Image-1574968625.jpg', 1, '2019-11-28 13:17:05', '2019-11-28 13:17:05'),
(9, 1, 9, 'TheAlImran-portfolio-Image-1574968653.jpg', 1, '2019-11-28 13:17:33', '2019-11-28 13:17:33'),
(10, 1, 9, 'TheAlImran-portfolio-Image-1574968677.jpg', 1, '2019-11-28 13:17:57', '2019-11-28 14:08:29'),
(11, 1, 11, 'TheAlImran-portfolio-Image-1574968706.jpg', 1, '2019-11-28 13:18:26', '2019-11-28 13:18:26'),
(12, 1, 1, 'TheAlImran-portfolio-Image-1574969059.png', 1, '2019-11-28 13:24:19', '2019-11-28 13:24:19'),
(13, 1, 3, 'TheAlImran-portfolio-Image-1574969094.jpg', 1, '2019-11-28 13:24:54', '2019-11-28 13:24:54'),
(14, 1, 7, 'TheAlImran-portfolio-Image-1574971864.png', 1, '2019-11-28 14:11:05', '2019-11-28 14:11:05'),
(15, 1, 11, 'TheAlImran-portfolio-Image-1574972070.jpg', 1, '2019-11-28 14:14:30', '2019-11-28 14:14:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Super-Admin' COMMENT 'Super Admin|Author|Editor',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=Inactive|1=Active|2=Blocked',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `image`, `password`, `phone`, `role`, `status`, `slug`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'noman141', 'nmnaba14@gmail.com', NULL, 'AlImran-noman141.jpg', '$2y$10$659BkRdwTIzZ3wwI3RY9XOm3dHRHvABf4530uvbnts3Vixg9Zun6q', '01750603409', 'Super-Admin', 1, 'noman141', NULL, '2019-11-26 07:30:56', '2019-11-26 07:31:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
